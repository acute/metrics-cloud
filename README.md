metrics-cloud
=============
## About

This is a collection of scripts used for deploying development, testing and
production systems relating to the [Tor
Metrics](https://metrics.torproject.org/) Team.

While the Tor Sysadmin Team is responsible for many of the systems that
production code is deployed on, a limited number of production hosts are also
run directly by the Metrics Team.

[![Build Status](https://travis-ci.org/torproject/metrics-cloud.svg?branch=master)](https://travis-ci.org/torproject/metrics-cloud)

As far as is possible the structure of these systems mirrors standard practice
by the Tor Sysadmin Team such that we can easily migrate systems to managed
infrastructure when desire and time present themselves.

## Onionperf deployment

To deploy an experimental Onionperf to a new machine, you will need to be able to log into it as a privileged user.
Deployment is done as follows:

1. Clone this repository, and change to the `ansible` directory. The files `production` and `development` in the `ansible` directory contain a list of all the Onionperf hosts and the variables used for their deployment.

2. Add the FQDN or IP address of the new host to the `production` or `development` file, as needed, under group `[onionperfs-exp]`. The `[onionperfs]` group is used for long running instances, while `[onionperfs-exp]` is used for experimental instances.

3. In the same file, edit the variables under `[onionperfs-exp:vars]`. These include the repositories, branches or tags to build Tor and Onionperf from, and the Onionperf command. The variables defined here apply to all experimental instances. Variables can also be specified inline on a per-instance basis, which will override the defaults.

4. Deploy the instance using the bootstrap script, using `-u` to specify the user to connect as:
```
./bootstrap.sh -u admin 1.2.3.4
```
Note: if you used a FQDN in step 2, you will need to use it here instead of the IP address. This installs Python on the remote machine and runs the playbook.
If the playbook fails to complete, it can be re-run with the following command:

```
ansible-playbook --limit 1.2.3.4 onionperfs.yml -i production
```

5. Make a pull request with your changes, to maintain a repo of all the deployed instances and their variables centrally.

The playbook can be run on all experimental instances to update the system packages and refresh the Onionperf and Tor installation:

```
ansible-playbook --limit onionperfs-exp onionperfs.yml -i production
```


This playbook is for the deployment of Onionperf instances only, and does not include the results in Collector.
